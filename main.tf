# Create VPC
module "Gilead-VPC" {
  source          = "./Modules/VPC"
  vpc-cidr        = var.vpc_cidr
  vpc-network-map = var.vpc_network_map
}

# Create Security Group
module "Gilead-SG" {
  source    = "./Modules/SECURITY-GROUP"
  alb-ports = var.alb_ports
  ec2-ports = var.ec2_ports
  vpc-id    = module.Gilead-VPC.vpc-id
}

# Create EC2 Server(s)
module "Gilead-EC2" {
  source               = "./Modules/EC2"
  instance-type        = var.instance_type
  instance-count       = var.instance_count
  subnet-ids           = module.Gilead-VPC.Public-subnet-IDs
  is-HA                = var.is_HA
  vpc-id               = module.Gilead-VPC.vpc-id
  instance-sec-grp-ids = [module.Gilead-SG.ec2-sg-ids]
  USER-DATA            = var.USER_DATA
  rsa-bits             = var.rsa_bits
  key-pair-name        = var.key_pair_name
  az                   = var.az_map
}

# Create ALB
module "Gilead-ALB" {
  source             = "./Modules/ALB"
  alb_name           = var.alb_name
  alb_tg_name        = var.alb_tg_name
  vpc-id             = module.Gilead-VPC.vpc-id
  alb_sec_groups     = [module.Gilead-SG.alb-sg-ids]
  alb_public_subnets = module.Gilead-VPC.Public-subnet-IDs
  alb_target_ids     = module.Gilead-EC2.instance-ids
  certificate_arn    = module.Gilead-ACM.acm-certificate-arn
  identifier         = var.identifier
  domain_name        = var.domain_name
  root_domain_name   = var.root_domain_name
  weight             = var.weight
}
# Create ACM
module "Gilead-ACM" {
  source           = "./Modules/ACM"
  domain_name      = var.domain_name
  root_domain_name = var.root_domain_name
}