# gitlab_cicd_terraform-

Create CI/CD Pipelines for Terraform in GitLab

This project will create:
- VPC with CIDR 10.0.0.0/16
- 3 subnets (public) with CIDR 10.0.1.0/24 and 10.0.2.0/24
- Amazon Linux 2 instance (t2.micro) across the Availability Zones


