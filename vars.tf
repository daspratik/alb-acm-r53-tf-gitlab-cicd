######################
# General Variables #
#####################
variable "aws_region" { type = string }
##################
# VPC Variables #
#################
variable "vpc_cidr" { type = string }
variable "vpc_network_map" {
  type = map(object(
    {
      public-subnet  = string
      private-subnet = string
      az             = string
    }
  ))
}
variable "az_map" { type = map(any) }
#############################
# Security Group Variables #
############################
variable "ec2_ports" {
  type = map(object(
    {
      description = string
      port        = number
      protocol    = string
      cidr_blocks = list(string)
    }
  ))
}

variable "alb_ports" {
  type = map(object(
    {
      description = string
      port        = number
      protocol    = string
      cidr_blocks = list(string)
    }
  ))
}

##########################
# RSA Key Pair Variables #
#########################
variable "rsa_bits" { type = number }
variable "key_pair_name" { type = string }

##################
# EC2 Variables #
#################
variable "instance_type" { type = string }
variable "is_HA" { type = bool }
variable "USER_DATA" { type = string }
variable "instance_count" { type = number }

##################
# ALB Variables #
#################
variable "alb_name" { type = string }
variable "alb_tg_name" { type = string }
variable "weight" { type = number }

##################
# ACM Variables #
#################
variable "root_domain_name" { type = string }
variable "domain_name" { type = string }
variable "identifier" { type = string }


