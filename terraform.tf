# Terraform Block
terraform {
  required_version = "~>1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.30.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">=3.1.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">=3.4.3"
    }
  }

  backend "s3" {
    bucket  = "374278-terraform-tfstate"
    key     = "gilead-cloudinfra"
    encrypt = "true"
    region  = "us-east-1"
    profile = "tf-admin"
  }
}
