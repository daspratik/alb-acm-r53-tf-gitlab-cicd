# Provider Block
provider "aws" {
  region  = var.aws_region
}

/*
allowed_account_ids = [data.aws_caller_identity.current_account.id]
  # Declaring AssumeRole
  assume_role {
    # The Role ARN is the Amazon Resource Name of the Cross Account IAM Role for this Account's Terraform CLI to Assume
    role_arn = var.role_arn
    # Declaring a STS session name
    session_name = var.session_name
  }
  */
