##################
# General Values #
#################
aws_region  = "us-west-2"
###############
# VPC Values #
##############
vpc_cidr = "10.60.0.0/16"
vpc_network_map = {
  "zone-A" = {
    public-subnet  = "10.60.1.0/24"
    private-subnet = "10.60.101.0/24"
    az             = "us-west-2a"
  }
  "zone-B" = {
    public-subnet  = "10.60.2.0/24"
    private-subnet = "10.60.102.0/24"
    az             = "us-west-2b"
  }
  "zone-C" = {
    public-subnet  = "10.60.3.0/24"
    private-subnet = "10.60.103.0/24"
    az             = "us-west-2c"
  }
}
az_map = {
  0 = "us-west-2a"
  1 = "us-west-2b"
  2 = "us-west-2c"
}
##########################
# Security Group Values #
#########################
ec2_ports = {
  "3389" = {
    description = "RDP"
    port        = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  "22" = {
    description = "SSH"
    port        = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  "80" = {
    description = "HTTP"
    port        = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  "443" = {
    description = "HTTPS"
    port        = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

alb_ports = {
  "80" = {
    description = "HTTP"
    port        = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  "443" = {
    description = "HTTPS"
    port        = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

###############
# EC2 Values #
##############
instance_type  = "t3.medium"
is_HA          = "true"
USER_DATA      = "USER_DATA.sh"
instance_count = 3

########################
# RSA Key Pair Values #
#######################
rsa_bits      = 4096
key_pair_name = "374278-Gilead-Failover-Routing"

###############
# ALB Values #
##############
alb_name    = "Gilead-ALB-Oregon"
alb_tg_name = "Gilead-ALB-Oregon-TG"
weight      = 100

###############
# ACM Values #
##############
root_domain_name = "devops-terraform.click"
domain_name      = "www.devops-terraform.click"
identifier       = "us-west-2"