output "subdomain-name" {
  value = var.domain_name
}

output "acm-certificate-arn" {
  value = aws_acm_certificate.acm_certificate.arn
}