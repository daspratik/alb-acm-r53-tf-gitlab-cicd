variable "instance-type" { type = string }
variable "vpc-id" { type = string }
variable "rsa-bits" { type = number }
variable "key-pair-name" { type = string }
variable "USER-DATA" { type = string }
variable "instance-sec-grp-ids" { type = list(string) }
variable "is-HA" { type = bool }
variable "az" {
  type = map(any)
}
variable "instance-count" { type = number }
variable "subnet-ids" { type = list(string) }
/*
variable "aws_region" { type = list(string) }
variable "is-env" { type = bool }
variable "aws_region" { type = map(any) }
variable "aws_account_id" { type = string }
variable "role_arn" { type = string }
variable "session_name" { type = string }
variable "az_count" { type = number }
variable "PUBLIC_KEY" { type = string }
variable "bastion_instance_type" { type = string }
variable "bastion-root-vol-size" { type = number }
variable "bastion-vol-type" { type = string }
variable "byte_length" { type = number }
variable "db_instance_type" { type = string }
variable "db_engine_version" { type = string }
variable "db_engine_mode" { type = string }
variable "secrets_rotation_interval" {type = number}
//variable "db_subnet_count" {type = number}
//variable "db_allocated_storage" {type = number}
//variable "ami_id" { type = map(any) }
*/
