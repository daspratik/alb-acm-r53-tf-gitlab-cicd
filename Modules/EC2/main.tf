# Data Block
data "aws_availability_zones" "instance_az" {
  state = "available"
}
data "aws_vpc" "instance_vpc" {
  id = var.vpc-id
}
data "aws_subnets" "ec2_public_subnets" {
  depends_on = [data.aws_vpc.instance_vpc]
  filter {
    name   = "vpc-id"
    values = [var.vpc-id]
  }
  filter {
    name   = "map-public-ip-on-launch"
    values = [true]
  }
  tags = {
    Tier = "374278-Public"
  }
}

data "aws_ami" "ami_id" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

locals {
  common_tags = {
    Name             = "374278-GileadTest"
    Owner            = "374278"
    Environment      = "Test"
    Application      = "Cloud-Infra"
    Create_date_time = formatdate("YYYY-MM-DD hh:mm:ss ZZZ", timestamp())
    Terraform        = "true"
  }
}

resource "aws_instance" "cloud_instance" {
  depends_on             = [aws_key_pair.ssh_key_pair]
  count                  = var.instance-count
  ami                    = data.aws_ami.ami_id.id
  instance_type          = var.instance-type
  key_name               = var.key-pair-name
  vpc_security_group_ids = var.instance-sec-grp-ids
  subnet_id              = element(var.subnet-ids, count.index)
  //subnet_id                   = element(tolist(data.aws_subnets.ec2_public_subnets.ids), count.index)
  associate_public_ip_address = true
  tags                        = local.common_tags
  disable_api_termination     = false
  user_data                   = file(var.USER-DATA)
  monitoring                  = true
  lifecycle {
    ignore_changes = [tags["Create_date_time"], ami]
  }
  timeouts {
    create = "10m"
  }
}

/*
# Create EBS Data volumes
resource "aws_ebs_volume" "ebs-vol01" {
  depends_on = [aws_instance.cloud_instance]
  count      = var.is-HA == true ? length(data.aws_subnets.ec2_public_subnets.ids) : 0
  //count             = var.is-HA == true ? length(data.aws_availability_zones.instance_az.names) : 0
  //availability_zone = data.aws_availability_zones.instance_az.names[count.index]
  availability_zone = lookup(var.az, count.index)
  size              = 15
  type              = "gp3"
  encrypted         = "true"
  final_snapshot    = false
}

resource "aws_ebs_volume" "ebs-vol02" {
  depends_on = [aws_instance.cloud_instance]
  count      = var.is-HA == true ? length(data.aws_subnets.ec2_public_subnets.ids) : 0
  //count             = var.is-HA == true ? length(data.aws_availability_zones.instance_az.names) : 0
  availability_zone = data.aws_availability_zones.instance_az.names[count.index]
  size              = 20
  type              = "gp3"
  encrypted         = "true"
  final_snapshot    = false
}

# Attach EBS Volumes to EC2 instances we created earlier
resource "aws_volume_attachment" "ebs-vol01-attachment" {
  depends_on = [aws_ebs_volume.ebs-vol01]
  count      = var.is-HA == true ? length(data.aws_subnets.ec2_public_subnets.ids) : 0
  //count        = var.is-HA == true ? length(data.aws_availability_zones.instance_az.names) : 0
  device_name  = "/dev/xvdh"
  instance_id  = aws_instance.cloud_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol01.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}

resource "aws_volume_attachment" "ebs-vol02-attachment" {
  depends_on = [aws_ebs_volume.ebs-vol02]
  count      = var.is-HA == true ? length(data.aws_subnets.ec2_public_subnets.ids) : 0
  //count        = var.is-HA == true ? length(data.aws_availability_zones.instance_az.names) : 0
  device_name  = "/dev/xvdj"
  instance_id  = aws_instance.cloud_instance.*.id[count.index]
  volume_id    = aws_ebs_volume.ebs-vol02.*.id[count.index]
  force_detach = "true"
  skip_destroy = "false"
}
*/
