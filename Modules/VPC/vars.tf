variable "vpc-cidr" { type = string }
variable "vpc-network-map" {
  type = map(object(
    {
      public-subnet  = string
      private-subnet = string
      az             = string
    }
  ))
}
/*
variable "aws_region" { type = string }
variable "aws_account_id" { type = string }
//variable "role_arn" { type = string }
//variable "session_name" { type = string }
//variable "aws_region" { type = list(string) }
variable "rsa-bits" { type = number }
variable "key-pair-name" { type = string }
variable "instance_type" { type = map(any) }
variable "is_env" { type = bool }
variable "sg_ports" { type = list(number) }
variable "az_count" { type = number }
variable "PUBLIC_KEY" { type = string }
variable "bastion_instance_type" { type = string }
variable "bastion-root-vol-size" { type = number }
variable "bastion-vol-type" { type = string }
variable "byte_length" { type = number }
variable "db_instance_type" { type = string }
variable "db_engine_version" { type = string }
variable "db_engine_mode" { type = string }
variable "secrets_rotation_interval" {type = number}
//variable "db_subnet_count" {type = number}
//variable "db_allocated_storage" {type = number}
//variable "ami_id" { type = map(any) }
*/
